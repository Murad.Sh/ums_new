package com.example.ums_new.service;

import com.example.ums_new.entity.TeacherEntity;
import com.example.ums_new.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    public List<TeacherEntity> getAllTeachers() {
        return teacherRepository.findAll();
    }

    public TeacherEntity getTeacherById(Long id) {
        return teacherRepository.findById(id).orElse(null);
    }

    public TeacherEntity saveTeacher(TeacherEntity student) {
        return teacherRepository.save(student);
    }

    public void deleteTeacher(Long id) {
        teacherRepository.deleteById(id);
    }
}
