package com.example.ums_new.exception;

public class StudentNotFoundException extends RuntimeException{
    public StudentNotFoundException(Long id) {
        super("Student with that ID" + id + "is not found");
    }
}
