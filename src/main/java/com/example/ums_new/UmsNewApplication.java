package com.example.ums_new;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmsNewApplication {

	public static void main(String[] args) {

		SpringApplication.run(UmsNewApplication.class, args);
	}

}
